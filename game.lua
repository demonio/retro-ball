local state = {}
state.first = false
local box = {}
local ball = {}
local player = {}
local timer = 60
local score = 0
local level = 1
local start = false
local field = {}
local delta = 0
local speed = 1000
local lives = 3
local width_var = 0
local height_var = 0
local over = false

function state.load()
  --love.keyboard.setKeyRepeat(true)
  width_var = width/16
  height_var = height/16
  box[1] = functions.generateBox((width/16)-2, (height/16)-2, {255,50,50,255}, -50)
  box[2] = functions.generateBox((width/16)-2, (height/16)-2, {255,255,50,255}, -50)
  box[3] = functions.generateBox((width/16)-2, (height/16)-2, {50,255,255,255}, -50)
  box[4] = functions.generateBox((width/16)-2, (height/16)-2, {50,255,50,255}, -50)
  player.image = functions.generateBox((width/8), height/16, {255,175,50,255}, -50)
  player.x = width/2-width/32
  player.y = height-height/16
  ball.x =  width/2-width/16
  ball.y =  height-(height/16)*2
  ball.ax = math.random(-250,250)
  ball.ay = -(600 + (level*50))
  ball.move = false
  start = true
  field[1] = {}
  for i in functions.range(2,5,1) do
    field[i] = {}
    for k in functions.range(1,16,1) do
      field[i][k] = {}
      field[i][k].id = math.random(1,4)
      field[i][k].image = box[field[i][k].id]
      field[i][k].x = (width_var*(k-1))+1
      field[i][k].y = (height_var*(i-1))+1
    end
  end
end

function state.update(dt)
  if start and love.window.hasFocus( ) then
    if (love.keyboard.isDown("left") or (love.mouse.isDown(1) and mousex<width/2) and ball.move ) and player.x > 0 then
      player.x = player.x - speed*dt
    end
    if (love.keyboard.isDown("right")  or (love.mouse.isDown(1) and mousex>width/2) and ball.move ) and player.x < (width-width/8) then
      player.x = player.x + speed*dt
    end
    if over then ball.move = false end
    if not ball.move then
      ball.x = player.x+(((width/8)/2))
      ball.y = player.y-height/128
    else
      timer = timer-dt
      ball.x = ball.x+ball.ax*dt
      ball.y = ball.y+ball.ay*dt
    end
    -- bounce check field
    local test = 0
    for i,v in pairs(field) do
      for k,n in pairs(field[i]) do
        test = test + 1
        if ball.x > n.x and ball.x < n.x+width/16 and ball.y > n.y and ball.y < n.y+height/16 then
          score = score + n.id
          field[i][k] = nil
          ball.ay = -ball.ay
          love.audio.play(functions.generateClickySound())
          if love.system.getOS() == "Android" then
            love.system.vibrate( 0.1 )
          end
        end
      end
    end
    if test == 0 then
      lives = lives + 1
      timer = 0
    end
    -- bounce check wall
    if ball.x < 0 or ball.x > width then
      ball.ax = -ball.ax
    end
    if ball.y < 0 then
      ball.ay = -ball.ay
      ball.y = 0
    end
    -- bounce check player
    if ball.x > player.x and ball.x < player.x+width/8 and ball.y > player.y and ball.y < player.y+height/16 then
      ball.ay = -600 + (level*50)
      ball.ax = -((((player.x+(((width/8)/2)))-ball.x)/(width/8))*1000)
      ball.y = player.y-height/128
    end
    -- not bounce under player
    if ball.y > height then
      lives = lives - 1
      ball.ax = math.random(-250,250)
      ball.ay = -(600 + (level*50))
      ball.move = false
    end
    -- next field
    if timer < 0 then
      level = level+1
      timer = 60-(5*(level-1))
      if timer < 10 then
        timer = 10
      end
      local tmp_field = {}
      for i,v in pairs(field) do
        for k,n in pairs(field[i]) do
          n.y = n.y+height/16
        end
        tmp_field[i+1] = v
      end
      tmp_field[1] = {}
      tmp_field[2] = {}
      for k in functions.range(1,16,1) do
        tmp_field[2][k] = {}
        tmp_field[2][k].id = math.random(1,4)
        tmp_field[2][k].image = box[tmp_field[2][k].id]
        tmp_field[2][k].x = (width_var*(k-1))+1
        tmp_field[2][k].y = (height_var*(2-1))+1
      end
      field = tmp_field
    end
    if lives == 0 then
      over = true
    end
  end
end

local function reset()
  instance = "menu"
  state.first = false
  timer = 60
  score = 0
  level = 1
  start = false
  delta = 0
  lives = 3
  width_var = 0
  height_var = 0
  over = false
  field = {}
end

function state.mousepressed(x, y, button)
  if button == 1 and not ball.move then
    if not over then
      ball.move = true
    else
      love.filesystem.write("score/"..os.time(), score)
      reset()
    end
  end
end

function state.keypressed(key)
  if key == "escape" then
    reset()
  end
  if key == "space" then
    if not over then
      ball.move = true
    else
      love.filesystem.write("score/"..os.time(), score)
      reset()
    end
  end
end

function state.draw()
  if start then
    love.graphics.draw(player.image, player.x, player.y)
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.circle("fill", ball.x, ball.y, width/128, 100)
      love.graphics.setColor(255, 255, 255, 255)
    for i,v in pairs(field) do
      for k,n in pairs(field[i]) do
        if n.image then
          love.graphics.draw(n.image,n.x,n.y)
        end
      end
    end
    love.graphics.print(language.score..":"..score,10,10)
    love.graphics.print(language.lives..":"..lives,width-default_font:getWidth(language.lives)-40,10)
    love.graphics.print(language.next_wave..functions.round(timer)..language.seconds,width/2-default_font:getWidth(language.next_wave..functions.round(timer)..language.seconds)/2,10)
    if over then
      love.graphics.setFont(default_big)
      love.graphics.print(language.game_over, width/2-default_big:getWidth(language.game_over)/2, height/2-default_big:getHeight())
      love.graphics.setFont(default_font)
    end
  end
end

return state
