local state = {}
state.first = false
local button = {}
local username = { text = language.username }
local password = { text = language.password }
local status = "OFFLINE"
local timer = 0
local scale = 1
local bg_bg = functions.generateBox(350, 180, {125,125,175}, 0)
local bg = functions.generateBox(340, 170, {200,200,250}, -75)
local registered = false

function state.load()
  if love.filesystem.isFile("player.username") then
    username = { text = love.filesystem.read("player.username") }
    password = { text = love.filesystem.read("player.password") }
 end
 if network.test() then
   status = "ONLINE"
 end
 love.keyboard.setKeyRepeat(true)
end

function state.update(dt)
  suit.Label(language.server_status..status, width/2-160,height/2-95, 320,30)
  button.username = suit.Input(username, width/2-160,height/2-55, 320,30)
  button.password = suit.Input(password, width/2-160,height/2-15, 320,30)
  button.login = suit.Button(language.login, width/2-160,height/2+25, 150,30)
  button.register = suit.Button(language.register, width/2+10,height/2+25, 150,30)
  if button.register.hit then
    if network.register (username.text, password.text) then
      timer = 300
      registered = true
    end
  end
  if button.login.hit then
    if network.login (username.text, password.text) then
      player = network.getUserData(username.text)
      if not love.filesystem.isFile("player.username") then
          love.filesystem.newFile("player.username")
          love.filesystem.write("player.username", username.text)
          love.filesystem.newFile("player.password")
          love.filesystem.write("player.password", password.text)
      else
          love.filesystem.write("player.username", username.text)
          love.filesystem.write("player.password", password.text)
      end
      love.keyboard.setKeyRepeat(false)
      instance = "menu"
    else
      timer = 300
    end
  end
  if timer > 0 then
    if registered then
      suit.Label(language.register_done, width/2-160,height/2+65, 320,30)
    else
      suit.Label(language.login_error, width/2-160,height/2+65, 320,30)
    end
    timer = timer - 1
  end
  if timer == 0 then
    registered = false
  end
  if width < height then
    scale = width/1920
  else
    scale = height/1080
  end
end

function state.draw()
  love.graphics.draw(images.background.grass, 0, 0,0,width/1920, height/1080)
  love.graphics.draw(bg_bg, width/2-175,height/2-110)
  love.graphics.draw(bg, width/2-170,height/2-105)
end

return state
