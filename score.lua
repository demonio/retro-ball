local state = {}
state.first = false
local score_list = nil
local best = 0
local bg_bg = functions.generateBox(320, 370, {50,50,50,50}, 0)
local bg = functions.generateBox(310, 360, {200,200,200,50}, -75)

function state.load()
  --get score values
  score_list = ""
  best = 0
  score = functions.recursiveEnumerate("score")
  limit = 1
  for i,v in pairs(score) do
    local value = love.filesystem.read(v)
    if limit < 11 then
      score_list = score_list..os.date("%x", i)..":"..value.."\n"
    end
    if tonumber(value) > best then
      best = tonumber(value)
    end
    limit = limit + 1
  end
end

function state.update(dt)
  suit.Label(language.best..best,  width/2-150,((height/2)-70*2)-50, 300,30)
  suit.Label(score_list,  width/2-150,((height/2)-70*2), 300,50)
end

function state.keypressed(key)
  if key == "escape" then
    instance = "menu"
    state.first = false
  end
end

function state.draw()
  love.graphics.draw(bg_bg, width/2-160,((height/2)-70*2)-10)
  love.graphics.draw(bg, width/2-155,((height/2)-70*2)-5)
end

return state
