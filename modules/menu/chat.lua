local state = {}
state.loaded = false
state.active = true
local timer = 1
local channel = "global"
local message = { text = "" }
local chat = {}
local font = love.graphics.getFont()
local slider = {value = 1, min = 1, max = 10}
local bg_bg = functions.generateBox(430, 260, {125,125,125}, -75)
local bg = functions.generateBox(420, 250, {200,200,200}, -75)

function state.load()
end

function state.update(dt)
  if timer > 0 then
    timer = timer - dt
  else
    chat = network.get("chat", "GET "..player.name)
    timer = 1
  end

  suit.Input(message, 10,height-40, 250,30)
  --suit.Label(chat,{align="left"}, 10,height-240, 420)
  if suit.Button(language.send, 270,height-40, 150,30).hit and string.len(message.text) > 0 then
    network.send("chat",channel.." "..player.name.." "..message.text)
    message = { text = "" }
  end
  if chat.line then
    if chat.line > 10 then
      slider.max = chat.line-10
      suit.Slider(slider,{vertical = true}, 400,height-240, 20,180)
    end
  end
end

function state.draw()
  love.graphics.draw(bg_bg, 0,height-260)
  love.graphics.draw(bg, 5,height-255)
  love.graphics.setColor(0, 0, 0)
  if chat.line then
    local lines = 1
    for i in functions.range(functions.round(slider.value),chat.line-1, 1) do
      local size = font:getWidth(chat.text[i])
      if size > 380 then
        lines = lines + 2
      else
        lines = lines + 1
      end
      if lines > 15 then
        break
      end
      love.graphics.printf( chat.text[i], 10, (height-50)-font:getHeight()*(lines-1), 380, "left" )

    end
  end
  love.graphics.setColor(255, 255, 255)
end

return state
